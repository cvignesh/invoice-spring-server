package com.project.invoice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.invoice.model.InvoiceItem;
import com.project.invoice.repository.InvoiceItemRepository;
import com.project.invoice.response.ErrorResponse;
import com.project.invoice.response.Response;

@RestController
@RequestMapping("/invoice-item")
public class InvoiceItemController {
	@Autowired
	private InvoiceItemRepository invoiceItemRepository;
	@GetMapping
	public Iterable<InvoiceItem> findAllInvoiceItems() {
		return invoiceItemRepository.findAll();
	}

	@GetMapping("/{id}")
	public Object findInvoiceItemById(@PathVariable int id)  {

		Optional<InvoiceItem> invoiceItem = invoiceItemRepository.findById(id);
		if (invoiceItem.isPresent()) {
			return invoiceItem.get();
		} else {
			return ErrorResponse.setResponse(HttpStatus.NOT_FOUND ,"No Data" , 404);
			
		}
	}
	@GetMapping("/invoice-no/{id}")
	public Object findInvoiceItemByInvoiceId(@PathVariable String id)  {
		
		List<InvoiceItem>invoiceItem = invoiceItemRepository.selectInvoiceItemByInvoiceNo(id);
		if (!invoiceItem.isEmpty()) {
			return invoiceItem;
		} else {
			return ErrorResponse.setResponse(HttpStatus.NO_CONTENT ,"No Data" , 204);
			
		}
	}
	

	@PostMapping 
	public Object addInvoiceItem(@RequestBody InvoiceItem InvoiceItem) {
		try {
			invoiceItemRepository.save(InvoiceItem);
			return Response.setResponse(HttpStatus.OK, "Invoice Added Succesfully", 200);
		} catch (IllegalArgumentException  e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "Argument Invalid", 400);
		}
	}

	@PutMapping
	public Object updateInvoiceItem(@RequestBody InvoiceItem InvoiceItem) {
		try {
			invoiceItemRepository.save(InvoiceItem);
			return Response.setResponse(HttpStatus.OK, "Invoice Added Succesfully", 200);
		} catch (IllegalArgumentException e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "No Sufficent Params", 400);
		}
	}

	@DeleteMapping
	public Object deleteInvoiceItem(@RequestParam int id) {
		try {
			invoiceItemRepository.deleteById(id);
			return Response.setResponse(HttpStatus.OK, "Invoice Deleted Succesfully", 200);
		} catch (IllegalArgumentException e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "Illegal Argument", 400);
		}
	}
	

}
