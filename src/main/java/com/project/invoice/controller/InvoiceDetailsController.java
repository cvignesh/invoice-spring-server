package com.project.invoice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.invoice.model.InvoiceDetails;
import com.project.invoice.repository.InvoiceDetailsRepository;
import com.project.invoice.response.ErrorResponse;
import com.project.invoice.response.Response;

@RestController
@RequestMapping("/invoice")
public class InvoiceDetailsController {
	@Autowired
	private InvoiceDetailsRepository invoiceDetailsRepository;
	@GetMapping
	public Iterable<InvoiceDetails> findAllInvoiceDetailss() {
		return invoiceDetailsRepository.findAll();
	}

	@GetMapping("/{id}")
	public Object findInvoiceDetailsById(@PathVariable int id)  {

		Optional<InvoiceDetails> InvoiceDetails = invoiceDetailsRepository.findById(id);
		if (InvoiceDetails.isPresent()) {
			return InvoiceDetails.get();
		} else {
			return ErrorResponse.setResponse(HttpStatus.NO_CONTENT,"No Data" , 204);
			
		}
	}

	@PostMapping 
	public Object addInvoiceDetails(@RequestBody InvoiceDetails InvoiceDetails) {
		try {
			invoiceDetailsRepository.save(InvoiceDetails);
			return Response.setResponse(HttpStatus.OK, "Invoice Added Succesfully", 200);
		} catch (IllegalArgumentException  e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "Argument Invalid", 400);
		}
	}

	@PutMapping
	public Object updateInvoiceDetails(@RequestBody InvoiceDetails InvoiceDetails) {
		try {
			invoiceDetailsRepository.save(InvoiceDetails);
			return Response.setResponse(HttpStatus.OK, "Invoice Added Succesfully", 200);
		} catch (IllegalArgumentException e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "No Sufficent Params", 400);
		}
	}

	@DeleteMapping
	public Object deleteInvoiceDetails(@RequestParam int id) {
		try {
			invoiceDetailsRepository.deleteById(id);
			return Response.setResponse(HttpStatus.OK, "Invoice Deleted Succesfully", 200);
		} catch (IllegalArgumentException e) {			
			e.printStackTrace();
			return ErrorResponse.setResponse(HttpStatus.BAD_REQUEST, "Illegal Argument", 400);
		}
	}

}
