package com.project.invoice.model;

public enum InvoiceStatus {
	Paid,
	Pending
}
