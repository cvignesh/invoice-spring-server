package com.project.invoice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_details")
public class InvoiceDetails {
	@Id
	int id;
	@Column(name = "user_id")
	int userId;
	@Column(name = "invoice_no")
	String invoiceNo;
	@Column(name = "bill_from_name")
	String billFromName;
	@Column(name = "bill_from_address" , columnDefinition = "TEXT")
	String billFromAddress;
	@Column(name = "bill_to_name")
	String billToName;
	@Column(name = "bill_to_address" , columnDefinition = "TEXT")
	String billToAddress;
	@Column(name = "invoice_amount")
	String invoiceAmount;
	@Column(name = "invoice_status")
	@Enumerated(EnumType.STRING)
	InvoiceStatus invoiceStatus;
	@Column(name = "invoice_date")
	private Date invoiceDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getBillFromName() {
		return billFromName;
	}
	public void setBillFromName(String billFromName) {
		this.billFromName = billFromName;
	}
	public String getBillFromAddress() {
		return billFromAddress;
	}
	public void setBillFromAddress(String billFromAddress) {
		this.billFromAddress = billFromAddress;
	}
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	public String getBillToAddress() {
		return billToAddress;
	}
	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}
	public String getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public InvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public InvoiceDetails() {}
	public InvoiceDetails(int id, int userId, String invoiceNo, String billFromName, String billFromAddress,
			String billToName, String billToAddress, String invoiceAmount, InvoiceStatus invoiceStatus,
			Date invoiceDate) {
		this.id = id;
		this.userId = userId;
		this.invoiceNo = invoiceNo;
		this.billFromName = billFromName;
		this.billFromAddress = billFromAddress;
		this.billToName = billToName;
		this.billToAddress = billToAddress;
		this.invoiceAmount = invoiceAmount;
		this.invoiceStatus = invoiceStatus;
		this.invoiceDate = invoiceDate;
	}
	
	
	
}
