package com.project.invoice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_items")
public class InvoiceItem {
	@Id
	int id;
	@Column(name = "invoice_no")
	String invoiceNo;
	String name;
	int qty;
	int price;
	int total;
	public InvoiceItem() {}
	public InvoiceItem(int id, String invoiceNo, String name, int qty, int price, int total) {
		super();
		this.id = id;
		this.invoiceNo = invoiceNo;
		this.name = name;
		this.qty = qty;
		this.price = price;
		this.total = total;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}
