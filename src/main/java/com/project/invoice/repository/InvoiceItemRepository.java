package com.project.invoice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.project.invoice.model.InvoiceItem;

public interface InvoiceItemRepository extends CrudRepository<InvoiceItem, Integer> {
	@Query(value = "select * from invoice_items where invoice_no = :invNo", nativeQuery = true)
	 List<InvoiceItem> selectInvoiceItemByInvoiceNo(String invNo);
}
