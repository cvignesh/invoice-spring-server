package com.project.invoice.repository;

import org.springframework.data.repository.CrudRepository;

import com.project.invoice.model.InvoiceDetails;

public interface InvoiceDetailsRepository  extends CrudRepository<InvoiceDetails, Integer> {
}
