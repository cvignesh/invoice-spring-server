## Invoice Spring Server 

Simple invoice generator application

MYSQL sample dump available in resources folder of the project

Structure of application: <br>
1. User Credentials:
    - For managing users
2. Invoice details:
    - Contails basic invoice details
3. Invoice items:
    - Items quantity and price for the invoice

API List: <br>
- User `CRUD` apis
- Invoice details `CRUD` apis
- Invoice items `CRUD` apis
- `GET- findInvoiceItemByInvoiceId / ` -

